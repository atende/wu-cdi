package info.atende.webutil.cdi;

import info.atende.webutil.jpa.Config;
import info.atende.webutil.jpa.ConfigDTO;
import info.atende.webutil.jpa.ConfigUtils;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.Properties;

/**
 * @author Giovanni Silva
 * Date: 8/27/13
 * Time: 9:29 PM
 */
public class InjectedConfigProvider {
    static final String FILE_NOTFOUND = "Arquivo {0} não encontrado";
    static final String MANDATORY_PARAM_MISSING = "No definition found for a mandatory configuration parameter : '{0}'";
    private static final String CONFIGDTO_CLASS_NOT_POSSIBLE_INJECT = "Cannot instantiate {0} to a instance of ConfigDTO";
    @PersistenceContext
    EntityManager em;
    @Produces
    @InjectedConfig
    public String injectConfig(InjectionPoint ip) throws IllegalStateException{
        InjectedConfig param = ip.getAnnotated().getAnnotation(InjectedConfig.class);
        if(param.key() == null || param.key().length() == 0){
            return param.defaultValue();
        }
        Properties props = null;
        String file = param.file();

        String value = null;
        URL url = null;
        url = Thread.currentThread().getContextClassLoader().getResource(file);
        if(url != null){
           props = new Properties();
            try{
                props.load(url.openStream());
                value = props.getProperty(param.key());
                if (value == null || value.trim().length() == 0) {

                    if (param.mandatory()){
                        throw new IllegalStateException(MessageFormat.format(MANDATORY_PARAM_MISSING, new Object[]{param.key()}));
                    }else if(param.defaultValue() == null || param.defaultValue().trim().length() == 0){
                        return null;
                    }else {
                        return param.defaultValue();
                    }

                }
            } catch (IOException | MissingResourceException e){
                if (param.mandatory()) throw new IllegalStateException(MessageFormat.format(MANDATORY_PARAM_MISSING, new Object[]{param.key()}));

            }
        }else{
            throw new IllegalStateException(MessageFormat.format(FILE_NOTFOUND, file));
        }

        return value;
    }
    @Produces
    @InjectedConfig
    public Config injectConfigEntity(InjectionPoint ip) throws IllegalStateException {
        InjectedConfig param = ip.getAnnotated().getAnnotation(InjectedConfig.class);
        if(param.key() == null || param.key().length() == 0){
            throw new IllegalStateException(MessageFormat.format(MANDATORY_PARAM_MISSING, new Object[]{param.key()}));
        }
        Config config = em.find(Config.class, param.key());
        if(config == null && param.mandatory()){
            throw new IllegalStateException(MessageFormat.format(MANDATORY_PARAM_MISSING, new Object[]{param.key()}));
        }

        return config;

    }
    @Produces
    @InjectedConfig
    public ConfigDTO injectConfigDTO(InjectionPoint ip) throws IllegalStateException{
        InjectedConfig param = ip.getAnnotated().getAnnotation(InjectedConfig.class);
        if(param.key() == null || param.key().length() == 0){
            throw new IllegalStateException(MessageFormat.format(MANDATORY_PARAM_MISSING, new Object[]{param.key()}));
        }
        if(param.type() == ConfigDTO.class){
            throw new IllegalStateException(MessageFormat.format(CONFIGDTO_CLASS_NOT_POSSIBLE_INJECT, new Object[]{param.type()}));
        }
        Config config = em.find(Config.class, param.key());
        if(config == null && param.mandatory()){
            throw new IllegalStateException(MessageFormat.format(MANDATORY_PARAM_MISSING, new Object[]{param.key()}));
        }
        if(config == null)
            return null;



        ConfigDTO configDTO = ConfigUtils.parseConfig(config, param.type()).get();
        return configDTO;
    }
}
