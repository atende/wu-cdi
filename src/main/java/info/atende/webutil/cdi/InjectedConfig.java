package info.atende.webutil.cdi;

import info.atende.webutil.jpa.ConfigDTO;

import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import static java.lang.annotation.ElementType.*;

/**
 * Anotação para injetar uma propriedade de qualquer arquivo de configuração presente no classpath
 * @author Giovanni Silva
 * Date: 8/27/13
 * Time: 9:19 PM
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD, PARAMETER})
public @interface InjectedConfig {
    /**
     * Bundle key
     * @return a valid bundle key
     */
    @Nonbinding String key() default "";

    /**
     * The file where look for properties. The file have to be saved in the classpath
     * @return a valid file or the default ApplicationConfig.properties
     */
    @Nonbinding String file() default "ApplicationConfig.properties";

    /**
     * Is it a mandatory property
     * @return true if mandatory
     */
    @Nonbinding boolean mandatory() default false;

    /**
     * Default Value if not provided
     * @return default or ""
     */
    @Nonbinding String defaultValue() default "";

    @Nonbinding Class<? extends ConfigDTO> type() default ConfigDTO.class;
}
