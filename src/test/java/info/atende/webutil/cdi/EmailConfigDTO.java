package info.atende.webutil.cdi;

import info.atende.webutil.jpa.ConfigDTO;

/**
 * @author Giovanni Silva
 *         Date: 8/16/14.
 */
public class EmailConfigDTO implements ConfigDTO {
    private String server;
    private Integer port;
    @Override
    public String configName() {
        return "email_config";
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
