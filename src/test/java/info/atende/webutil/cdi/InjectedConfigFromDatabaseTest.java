package info.atende.webutil.cdi;

import info.atende.webutil.jpa.Config;
import info.atende.webutil.jpa.ConfigDTO;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;
import java.util.HashMap;
import java.util.Map;

/**
 * Criado por Giovanni Silva <giovanni@atende.info>
 * Date: 8/27/13
 * Time: 9:49 PM
 */
@RunWith(Arquillian.class)
public class InjectedConfigFromDatabaseTest {
    @Deployment
    public static Archive<?> createTestArchive(){
        Archive<?> archive = ShrinkWrap.create(WebArchive.class)
                .addClass(InjectedConfig.class)
                .addClass(InjectedConfigProvider.class)
                .addClass(EmailConfigDTO.class)
                .addPackage(info.atende.webutil.jpa.Config.class.getPackage())
                .addAsResource(new StringAsset("exist=Existe"), "Config.properties")
                .addAsResource("ApplicationConfig.properties")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource("jbossas-ds.xml")
                .addAsWebInfResource("META-INF/beans.xml", "beans.xml");
        System.out.println(archive.toString(true));
        return archive;
    }

    @PersistenceContext
    EntityManager em;
    @Inject
    UserTransaction utx;

    @Inject
    @InjectedConfig(key = "email_config")
    Instance<Config> emailConfig;
    @Inject
    @InjectedConfig(key = "nonexist")
    Instance<Config> nullConfig;
    @Inject
    @InjectedConfig(key = "email_config", type = EmailConfigDTO.class)
    Instance<ConfigDTO> emailConfigDTO;


    @Before
    public void preparePersistenceTest() throws Exception{
        clearData();
        insertData();
        startTransaction();
    }
    @After
    public void commitTransaction() throws Exception {
        utx.commit();
    }

    private void clearData() throws Exception {
        utx.begin();
        em.joinTransaction();
        System.out.println("Dumping old records...");
        em.createQuery("delete from Config").executeUpdate();
        utx.commit();
    }

    private void insertData() throws Exception {
        utx.begin();
        em.joinTransaction();
        System.out.println("Inserting records...");
        Map<String,String> values = new HashMap<>();
        values.put("server", "server_value");
        values.put("port","456");
        Config config = new Config("email_config", values);
        em.persist(config);
        utx.commit();
        // clear the persistence context (first-level cache)
        em.clear();
    }

    private void startTransaction() throws Exception {
        utx.begin();
        em.joinTransaction();
    }


    @Test
    public void shouldInjectEmailConfig(){
        Config config = emailConfig.get();
        Assert.assertNotNull("Config should not be null",config);
        Assert.assertEquals("value for key server should be retrieved", "server_value", config.getValues().get("server"));
        Assert.assertEquals("value key for port should be retrieved", "456", config.getValues().get("port"));
    }
    @Test
    public void shouldInjectNull(){
        Config config = nullConfig.get();
        Assert.assertNull(config);
    }
    @Test
    public void shouldInjectConfigDTO(){
        EmailConfigDTO configDTO = (EmailConfigDTO) emailConfigDTO.get();
        Assert.assertNotNull(configDTO);
        Assert.assertEquals("server_value", configDTO.getServer());
        Assert.assertEquals(456, configDTO.getPort().intValue());
    }

}
