package info.atende.webutil.cdi;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

/**
 * Criado por Giovanni Silva <giovanni@atende.info>
 * Date: 8/27/13
 * Time: 9:49 PM
 */
@RunWith(Arquillian.class)
public class InjectedConfigTest {
    @Deployment
    public static Archive<?> createTestArchive(){
        Archive<?> archive = ShrinkWrap.create(JavaArchive.class)
                .addClass(InjectedConfig.class)
                .addClass(InjectedConfigProvider.class)
                .addAsResource(new StringAsset("exist=Existe"),"Config.properties")
                .addAsResource("ApplicationConfig.properties")
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
        System.out.println(archive.toString(true));
        return archive;
    }
    @Inject
    @InjectedConfig(key = "importKey")
    String importKey;
    @Inject
    @InjectedConfig(key = "nonExist")
    String nullValue;

    @Inject
    @InjectedConfig(key = "nonExist", defaultValue = "Default")
    String defaultValue;

    @Inject
    @InjectedConfig(key = "exist", file = "Config.properties")
    String config;

    @Test
    public void testPropertyInjection(){
        Assert.assertNotNull(importKey);
        Assert.assertEquals("TESTE", importKey);
    }
    @Test
    public void testPropertyNull(){
       Assert.assertNull(nullValue);
    }
    @Test
    public void testDefaultValue(){
       Assert.assertEquals("Default", defaultValue);
    }
    @Test
    public void testFileProperty(){
       Assert.assertEquals("Existe", config);
    }
}
