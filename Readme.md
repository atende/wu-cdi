A library for CDI - Context And Dependency Injection
=================================================================

## Installing

Just use maven or gradle

**groupId:** info.atende.webutil

**artifactId:** cdi

Maven Dependency (don't forget the version)

    <dependency>
        <groupId>info.atende.webutil</groupId>
        <artifactId>cdi</artifactId>
    </dependency>

Gradle Dependency
    
    compile 'info.atende.webutil:cdi:$wuCdiVersion'
 
## Documentation

[http://wiki.atende.info/display/wu](http://wiki.atende.info/display/wu)

## Filling Bugs

Please file bugs in [Jira](http://projetos.atende.info/browse/wu)

## I want help

Very good. Read [contributing](contributing)